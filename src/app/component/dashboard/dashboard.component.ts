import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { CartService } from 'src/app/service/cart.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public productList: any;
  public filterCategory : any
  public isSelected: boolean = false;
  searchKey:string ="";

  constructor( private api:ApiService, private cartService:CartService,private router: Router) { }

  ngOnInit(): void {

    this.api.getProduct().subscribe( res => {
       this.productList = res;
       this.filterCategory = res;

       this.productList.forEach((a:any) => {
        console.log(a.vehicaltype);
        if(a.vehicaltype ==="fule"){
          a.vehicaltype ="fule";
        }else{
          a.vehicaltype = "electrical";
        }
        
      });
      console.log(this.productList)
    })

    this.cartService.search.subscribe((val:any)=>{
      this.searchKey = val;
    })
  }

  filter(vehicaltype:string){
    this.filterCategory = this.productList.filter((a:any)=>{
         if(a.vehicaltype == vehicaltype || a.vehicaltype ==''){
             return a;
         }
    })
  }


  addToCart(item:any){
   this.cartService.addToCart(item);
  }

  clickKnowMore(item:any){

     this.router.navigateByUrl("/product-detail");
  }

  addToWishlistItem(item:any){
  item.isSelected = true;
   this.cartService.addToWishlistItem(item);
  }

}
