import { Component, Input, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {

  public productList:any;
  constructor(private api:ApiService) { }

  ngOnInit(): void {
  
    this.api.getProduct().subscribe(res =>{
    this.productList = res;
     });

  }

}
