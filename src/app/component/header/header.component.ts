import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ApiService } from 'src/app/service/api.service';
import { CartService } from 'src/app/service/cart.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  // @Input() productList:any = [];
   public productList:any;
  public yearOptions: any;
  public languageArray = ['english', 'gujarati', 'hindi'];
  public totalItem: number = 0;
  public wishlistTotalItem: number = 0;

  public searchTerm !: string;
  @Output() selectionChange = new EventEmitter<number>();
 
  constructor(private translate: TranslateService, private api:ApiService,private cartService:CartService) {
    this.translate.use('english');
   }

  ngOnInit(): void {
    this.api.getProduct().subscribe( res => {
      this.productList = res;
      this.yearOptions = this.productList?.map((a:any) => a.year);
      this.yearOptions = this.yearOptions.filter((item:any,
        index:any) => this.yearOptions.indexOf(item) === index);
      //this.yearOptions = this.productList.year;
   })
 
  
   this.cartService.getProduct().subscribe(res =>{
     //this.totalItem = res.length;
     this.wishlistTotalItem = res.length;
   })
  }


  changeLang(lang: string) {
    this.translate.use(lang);
  }

  yearChangeClick(event:any){
    this.selectionChange.emit(event.target.value);
    console.log(this.selectionChange);
    //this.cartService.search.next(this.selectionChange);
  }


}
